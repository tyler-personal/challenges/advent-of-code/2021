object Day1 extends App:
  val data = os.read(os.pwd / os.up / "data" / "day1.txt").split("\n").map(_.toInt).toList
  List(part1, part2).map(_(data)).foreach(println)

  def part1(data: List[Int]) =
    data.zip(data.tail).count(_ < _)

  def part2(data: List[Int]) =
    val windowSums = data.lazyZip(data.tail).lazyZip(data.tail.tail).map(_ + _ + _)
    part1(windowSums)

