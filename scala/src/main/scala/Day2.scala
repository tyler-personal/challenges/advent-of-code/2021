import scala.util.chaining._

object Day2 extends App:
  val data = os
    .read(os.pwd / os.up / "data" / "day2.txt")
    .split("\n")
    .map(line => line.split(" ").pipe(x => (x(0), x(1).toInt)))
    .toList
  List(part1, part2).map(_(data)).foreach(println)

  def part1(data: List[(String, Int)]) =
    data.foldLeft((0, 0)) {
      case ((horizontal, depth), (direction, number)) => direction match {
        case "forward" => (horizontal + number, depth)
        case "down"    => (horizontal, depth + number)
        case "up"      => (horizontal, depth - number)
      }
    }.pipe(_ * _)

  def part2(data: List[(String, Int)]) =
    data.foldLeft((0, 0, 0)) {
      case ((horizontal, depth, aim), (direction, number)) => direction match {
        case "forward" => (horizontal + number, depth + (aim * number), aim)
        case "down"    => (horizontal, depth, aim + number)
        case "up"      => (horizontal, depth, aim - number)
      }
    }.pipe((h, d, _) => h * d)

