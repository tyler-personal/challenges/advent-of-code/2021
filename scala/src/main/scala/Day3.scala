import cats._
import cats.implicits._
import scala.util.chaining._

object Day3 extends App:
  val data = os.read(os.pwd / os.up / "data" / "day3.txt").split("\n").toList
//  println(part1(data))
  //List(part1, part2).map(_(data)).foreach(println)
 

  extension [A](xs: Iterable[A])
    def mostCommon = xs.groupBy(identity).maxBy(_._2.size)._1
    def leastCommon = xs.groupBy(identity).minBy(_._2.size)._1

  def part1(data: List[String]) =
    val gamma = data.transpose.map(_.mostCommon).mkString.pipe(Integer.parseInt(_, 2))
    val epsilon = data.transpose.map(_.leastCommon).mkString.pipe(Integer.parseInt(_, 2))
    gamma * epsilon

  def part2(data: List[String]) = ???
